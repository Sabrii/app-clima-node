
const axios=require('axios');

const getLugar=async (direccion)=>{
    const tranformar=encodeURI(direccion)
    
    const instance = axios.create({
            baseURL: `https://devru-latitude-longitude-find-v1.p.rapidapi.com/latlon.php?location=${tranformar}`,
            headers: {'x-rapidapi-key': '4850250658mshe2de0fdbb5a57bcp171dcfjsn527cb98c2c52'}
        });
        const instancia  = await  instance.get()
        if (instancia.data.Results.length===0)
        {
             throw new Error(`No hay resultados para la direccion ${direccion}`);
        }
        else
        {   
            const resultados=instancia.data.Results[0];
            //console.log(resultados)
            nombre=resultados.name;
            latitud=resultados.lat;
            longitud=resultados.lon;

        }
        return {
            nombre,
            latitud,
            longitud 
        }
       
    }

module.exports={
    getLugar 
}    
