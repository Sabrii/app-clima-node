const apiclima=require('axios');
const GetClima= async (lat, long)=>{
    let resp=await apiclima.get(`https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${long}&appid=984180ce4be9cf70e9b213a44ac01e75&units=metric`);
    return resp.data.main.temp;

}
module.exports={
    GetClima
}

